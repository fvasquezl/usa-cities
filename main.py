import turtle
import pandas
FONT = ('Arial', 8, 'normal')
data = pandas.read_csv('./50_states.csv')
all_states = data.state.to_list()
guessed_states = []

screen = turtle.Screen()
screen.title('U.S. States Game')
screen.setup(740,510)
image = "blank_states_img.gif"
screen.addshape(image)
turtle.shape(image)
screen.tracer(0)


def write_state(x, y, my_state):
    t = turtle.Turtle()
    t.hideturtle()
    t.penup()
    t.goto(x, y)
    t.write(my_state, align="center", font=FONT)


while len(guessed_states) < 50:
    answer_state = screen.textinput(
        title=f"{len(guessed_states)}/50 States correct",
        prompt="What's the state's name?"
    )
    if not answer_state:
        missing_states = []
        for state in all_states:
            if state not in guessed_states:
                missing_states.append(state)
        states_to_learn = {
            'state': missing_states
        }
        new_data = pandas.DataFrame(states_to_learn)
        new_data.to_csv('states_to_learn.csv')
        break
    else:
        if answer_state.title() in all_states:
            state_data = data[data.state == answer_state.title()]
            guessed_states.append(answer_state)
            write_state(int(state_data.x), int(state_data.y), state_data.state.item())
        else:
            print("That state doesn't exist")




